# 算例介绍

使用求解器`laplacianFoam`求解法兰热传导问题。求解的传热方程如下：
$$
\frac{\partial T}{\partial t} = \nabla\cdot ( D_T \nabla T)
$$
其中$T$为温度场，$D_T$为导热系数，其值为$4\times10^{-5} \; {\rm m^2/s}$。

# 物理模型简图

说明：能直观看出初始条件、边界条件、几何尺寸等，推荐工具：AxGlyph、ppt、latex下的tikz等。如果可以，请将绘图的源文件存放于算例的source目录中。

透明度设置为0.9，各面在图中给出标注，内场的初始温度为$273\;{\rm K}$，边界面patch2的温度固定为$273\;{\rm K}$，变截面patch4的温度固定为$573\;{\rm K}$，温度差导致热传导。

![模型](source/laplacianFoam_flange-model_with_label.png)

# 网格

![网格](source/laplacianFoam_flange-mesh.png)



# 结果

说明：对于瞬态算例，请将动画文件存于source文件夹中，如不熟悉动画制作，可参考项目wiki说明[点击跳转](https://gitee.com/xfygogo/of-tutorial-gallary/wikis/%E5%90%8E%E5%A4%84%E7%90%86-%E5%8A%A8%E7%94%BB%E5%88%B6%E4%BD%9C?sort_id=3562815)。建议保存ParaView的State文件至算例source目录中：File->Save State。

初始t=0温度场

<img src="source/laplacianFoam_flange-T0.png" style="zoom:50%;" />

t=30温度场

<img src="source/laplacianFoam_flange-T30.png" style="zoom:50%;" />



# 计算流程

说明：类似Allrun脚本的内容，可包括拷贝算例、网格生成、网格划分、（并行）求解、后处理等。

```sh
# 网格转换
ansysToFoam flange.ans -scale 0.001 > log.ansysToFoam
# 求解
laplacianFoam > log.laplacianFoam
# 将OpenFOAM数据转换成Ensight格式
foamToEnsight
foamToEnsightParts
# 将OpenFOAM数据转换成VTK
foamToVTK
# 可视化
paraFoam --data=VTK/flange_..vtk
```

# 计算耗时

算例运行平台：Inter(R) Core(TM) i5-9500 [CPU@3.00GHz](mailto:CPU@3.00GHz)

进程数：1

耗时：2.31s

# 扩展内容

说明：以下为可选内容。

## 相关算例

[mesh/foamyHexMesh_flange](../../mesh/foamyHexMesh_flange)

[mesh/snappyHexMesh_flange](../../mesh/snappyHexMesh_flange)

## 文献对比



## 参考资料